﻿(*
 * Delphi Markdown Docuement Generator
 *
 * PLATFORMS
 *   Windows, macOS
 *
 * LICENSE
 *   Copyright (C) 2019 pik. (Twitter: @pik)
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * 2019/03/10 Version 1.0.0
 * 2019/03/14 Version 1.1.0
 * 2019/03/15 Version 1.1.1
 * 2019/03/15 Version 1.1.2
 *)

(**
  # Delphi Markdown Documente Generator (DelphiMDDoc.exe)
    マークダウン形式のコメントからドキュメントを生成します
*)

program DelphiMDDoc;

{$APPTYPE CONSOLE}
{$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
{$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$SetPEFlags 1} // IMAGE_FILE_RELOCS_STRIPPED = 1

{$R *.res}

uses
  System.SysUtils
  , uGenerator in 'uGenerator.pas'
  ;

const
  (**
    ## 定数
    :: const
    * USAGE
      パラメータ無しで呼び出したりパラメータが不正な場合に表示されるメッセージ
  *)
  USAGE =
    sLineBreak +
    'Delphi MDDoc 1.1.3' + sLineBreak +
    '  Generate Markdown style documentation from Delphi source code.'
      + sLineBreak +
    '  Copyright (C) pik. 2019, 2022.' + sLineBreak +
    sLineBreak +
    '[Repository]' + sLineBreak +
    '  https://bitbucket.org/freeonterminate/delphimddoc/' + sLineBreak +
    sLineBreak +
    '[USAGE]' + sLineBreak +
    '  DelphiMDDoc FilePath or DirectoryPath' + sLineBreak +
    sLineBreak +
    '[SAMPLE]' + sLineBreak +
    '  Sample1: Generate all of files (*.pas, *.dpr, *.inc).' + sLineBreak +
    '    DelphiMDDOc D:\Source\Sample\' + sLineBreak +
    '  Sample2: Generate specific file.' + sLineBreak +
    '    DelphiMDDOc D:\Source\Sample\test.pas' + sLineBreak
    ;

(**
  ## 手続き群
  * ShowUsage
    USAGE を表示します
*)
procedure ShowUsage;
begin
  Write(USAGE);
end;

(**
  ## 本体
    パラメータによって USAGE を表示するか、ファイルを生成する
*)
begin
  if ParamCount = 0 then
    ShowUsage
  else
  begin
    var Gen := TGenerator.Create;
    try
      var Path := ParamStr(1);
      if not Gen.Generate(Path) then
        ShowUsage;
    finally
      Gen.DisposeOf
    end;
  end;
end.
