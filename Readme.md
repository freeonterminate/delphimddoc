﻿# Delphi Markdown Document Generator

Generate Markdown style documentation from Delphi source code.  
Delphiのソースコードから Markdown 形式のドキュメントを生成します。  

Please read this article for more details (but it is in Japanese)  
https://qiita.com/pik/items/c229d2db3ebf78dcaa99

## Usage
**1.Specify the file name**

```bat
DelphiMdDoc D:\Sample\Foo.pas
```

Generate ```Foo.md``` .

**2.Specify the folder name**

```bat
DelphiMdDoc D:\Sample
```
If ```Foo.dpr```, ```Bar.pas```, ```Baz.inc``` exists in the specified folder, ```Foo.md```, ```Bar.md```, ```Baz.md``` is generated.

## Comment Format
It is Comment Format. You can write any markdown text between ```(**``` and ```*)```

```pascal
(**
  Write Markdown here
*)
```

#### example 1: With Parameter definer. (See also "Special symbol --" for details)

```markdown
(**
  ## Foo
    Foo is function for something 
	-- Param1
	  Param1 is Parameter 
	-- Param2
	  Param2 is Parameter
*)
```

#### example 2: Image file URL can handle like as usual markdown.

```markdown
(**
  ## Image
  ![Image](https://foo.bar/baz.png)
*)
```

### Special symbol --
```--``` is parameter definer.

For example, if you wrote as following code...

```pascal
(**
  * Foo
    procedure of Foo
    -- Param1
      It is description for Param1
*)
procedure Foo(const Param1: String);
begin
end;
```

DelphiMDDoc generates following markdown.

```markdown
* Foo
procedure of Foo
    * Param1
It is description for Param1
```



### Special symbol ::

```::``` is grouping identifier.

For example, if NOT to use ```::``` as following,

```pascal
(**
  # Foo
  description of Foo
*)
procedure Foo;
begin
end;

(**
  # Bar
  description of Bar
*)
procedure Bar;
begin
end;

(**
  # Baz
  description of Baz
*)
procedure Baz;
begin
end;
```

DelphiMDDoc generates from it with original orderd as following.

```markdown
# Foo
description of Foo
# Bar
description of Bar
# Baz
description of Baz
```

But using ```::``` identifier as following...

```pascal
(**
  # Foo::group_A
  description of Foo, it is in group_A
*)
procedure Foo;
begin
end;

(**
  # Bar
  description of Bar, it is out of any group.
*)
procedure Bar;
begin
end;

(**
  ::group_A
  # Baz
  description of Baz, it is also in group_A, so it has moved to near Foo.
*)
procedure Baz;
begin
end;
```

DelphiMDDoc generates from it with changing  order as following.

```markdown
# Foo
description of Foo, it is in group_A
# Baz
description of Baz, it is also in group_A, so it has moved to near Foo.
# Bar
description of Bar, it is out of any group.
```

## Sample screenshot for DelphiMDDoc

Please see this sample image by the original scale because the inline image is too much smaller to see details. (ex. open a context menu on the image and click "Open image in a new tab")  
画像が潰れてしまっているので、原寸大でご覧ください（右クリック「新しいタブで画像を開く」など）  

![Sample](sample.png)

# Special Thanks
Thanks to @kaz_inoue (Twitter) helping to translate this readme to English.

# License
Copyright (C) 2019 pik. (Twitter: @pik)  
Released under the MIT license  
http://opensource.org/licenses/mit-license.php
